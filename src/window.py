# Copyright 2018  Carmen Bianca Bakker <carmen@carmenbianca.eu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0+

from gettext import gettext as _

from gi.repository import Gio, GLib, Gtk

from .widgets.characterwidget import CharacterBox
from .widgets.resultboxrow import ResultBoxRow
from .die import roll_initiative


@Gtk.Template(resource_path='/org/gnome/Initiative-Tool/ui/window.ui')
class InitiativeToolWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'InitiativeToolWindow'

    _header = Gtk.Template.Child()
    _characterbox = Gtk.Template.Child()
    _instructionlabel = Gtk.Template.Child()
    _refreshbutton = Gtk.Template.Child()
    _resultbox = Gtk.Template.Child()
    _resultframe = Gtk.Template.Child()
    _resultstack = Gtk.Template.Child()
    _rollbutton = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self._settings = Gio.Settings.new('org.gnome.Initiative-Tool')

        self.model = Gtk.ListStore(str, int)
        self._load_model()

        self._characterbox.set_model(self.model)

        self._suggest_roll(self.model, None, None)

        self._accel = Gtk.AccelGroup()
        self.add_accel_group(self._accel)
        # TRANSLATORS: This is a shortcut for _adding an extra row.
        key, mod = Gtk.accelerator_parse(_('<Alt>a'))
        self._characterbox._add_button.add_accelerator(
            'clicked', self._accel, key, mod, Gtk.AccelFlags.VISIBLE)

        self.model.connect('row-changed', self._suggest_roll)
        self.model.connect('row-deleted', self._suggest_roll)
        self.model.connect('row-changed', self._update_settings)
        self.model.connect('row-deleted', self._update_settings)
        self.model.connect('row-inserted', self._update_settings)
        self._refreshbutton.connect('clicked', self._refresh)
        self._rollbutton.connect('clicked', self._roll_initiative)

    def _load_model(self):
        names = self._settings.get_value('character-names')
        modifiers = self._settings.get_value('character-modifiers')
        for name, modifier in zip(names, modifiers):
            self.model.append([name, modifier])

    def _refresh(self, _):
        for _ in self.model:
            iter = self.model.get_iter(Gtk.TreePath([0]))
            self.model.remove(iter)
        self.model.append(['', 0])
        # TODO: Change this to better refresh the state
        self._roll_initiative(None)

    def _roll_initiative(self, _):
        characters = [(item[0], item[1]) for item in self.model if item[0]]
        order = roll_initiative(characters)

        if not order:
            # TODO: Warn user maybe.
            self._resultstack.props.visible_child = self._instructionlabel
            return

        children = self._resultbox.get_children()
        for child in children:
            self._resultbox.remove(child)
            child.destroy()

        for item in order:
            row = ResultBoxRow(item[0], item[1])
            self._resultbox.add(row)
            row.show()

        self._resultstack.props.visible_child = self._resultframe

    def _suggest_roll(self, model, _1=None, _2=None):
        context = self._rollbutton.get_style_context()
        for item in model:
            if item[0]:
                context.add_class('suggested-action')
                break
        else:
            context.remove_class('suggested-action')

    def _update_settings(self, model, _1=None, _2=None):
        self._settings.set_value(
            'character-names',
            GLib.Variant('as', [item[0] for item in self.model]))
        self._settings.set_value(
            'character-modifiers',
            GLib.Variant('ai', [item[1] for item in self.model]))
