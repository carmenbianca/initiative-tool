# Copyright 2018  Carmen Bianca Bakker <carmen@carmenbianca.eu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0+

import random
from typing import Sequence, Set, Tuple


def roll_die(faces: int = 20, advantage: int = 0) -> int:
    """Roll a die.  If *advantage* is negative, the die is rolled twice, and
    the lowest number is picked.  If *advantage* is positive, the die is rolled
    twice, and the highest number is picked.  If *advantage* is 0, the die is
    only rolled once.

    :param faces: Amount of faces on the die.
    :param advantage: Whether the die is rolled with advantage, disadvantage,
        or neither.
    :return: The roll.
    """
    result = random.randint(1, faces)
    if advantage:
        second = random.randint(1, faces)
    if advantage < 0:
        result = result if result < second else second
    elif advantage > 0:
        result = result if result > second else second
    return result


def roll_initiative(
        characters: Set[Tuple[str, int]]) -> Sequence[Tuple[str, int]]:
    """Roll initiative for all characters, and return a sorted list of
    characters with their rolls.
    """
    results = [
        (character[0], roll_die(faces=20) + character[1])
        for character in characters]
    # Shuffle results to randomise equal rolls.
    random.shuffle(results)
    results.sort(key=lambda x: x[1], reverse=True)
    return results
