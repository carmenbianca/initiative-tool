# Copyright 2018  Carmen Bianca Bakker <carmen@carmenbianca.eu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0+

import sys
import gi
from gettext import gettext as _

gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')

from gi.repository import Gdk, Gio, Gtk, GLib

from .window import InitiativeToolWindow


class Application(Gtk.Application):
    def __init__(self, version=None):
        super().__init__(application_id='org.gnome.Initiative-Tool',
                         flags=Gio.ApplicationFlags.FLAGS_NONE)

        self._window = None
        self._version = version
        self._application_id = 'org.gnome.Initiative-Tool'
        self._application_name = _('Initiative Tool')

        self.props.resource_base_path = "/org/gnome/Initiative-Tool"
        GLib.set_application_name(self._application_name)
        GLib.set_prgname(self._application_id)

        css_provider = Gtk.CssProvider()
        css_provider.load_from_resource(
            '/org/gnome/Initiative-Tool/org.gnome.Initiative-Tool.css')
        screen = Gdk.Screen.get_default()
        style_context = Gtk.StyleContext()
        style_context.add_provider_for_screen(
            screen, css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

    def _build_app_menu(self):
        action_entries = [
            ('about', self._about),
            ('quit', self.quit),
        ]

        for action, callback in action_entries:
            simple_action = Gio.SimpleAction.new(action, None)
            simple_action.connect('activate', callback)
            self.add_action(simple_action)

    def _about(self, action, param):
        about = Gtk.AboutDialog()

        about.props.logo_icon_name = 'image-missing'
        about.props.comments = _('A tool for keeping track of initiative')
        about.props.copyright = _('Copyright © 2018 Carmen Bianca Bakker')
        about.props.license_type = Gtk.License.GPL_3_0
        about.props.modal = True
        about.props.version = self._version
        about.props.program_name = self._application_name
        about.props.translator_credits = _('translator-credits')
        about.props.authors = ['Carmen Bianca Bakker']
        about.props.transient_for = self._window
        about.connect('response', lambda dialog, parameter: dialog.destroy())
        about.show()

    def quit(self, action=None, param=None):
        self._window.destroy()

    def do_startup(self):
        Gtk.Application.do_startup(self)

        self._build_app_menu()

    def do_activate(self):
        win = self.props.active_window
        if not win:
            win = InitiativeToolWindow(application=self)
        self._window = win
        self._window.present()


def main(version):
    app = Application(version)
    return app.run(sys.argv)
