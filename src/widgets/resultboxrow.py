# Copyright 2018  Carmen Bianca Bakker <carmen@carmenbianca.eu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0+

from gettext import gettext as _

from gi.repository import GObject, Gtk


class ResultBoxRow(Gtk.ListBoxRow):

    def __init__(self, name=None, result=None, **kwargs):
        super().__init__(**kwargs)

        self._box = Gtk.Box()
        self._box.props.margin_left = 6
        self._box.props.margin_right = 6
        self._box.props.border_width = 12
        self._box.props.spacing = 24

        self._name_label = Gtk.Label()
        self._name_label.props.hexpand = True
        self._name_label.props.xalign = 0
        self._name_label.set_name('name-label')
        if name:
            self.set_name_text(name)
        self._box.add(self._name_label)
        self._name_label.show()

        self._result_label = Gtk.Label()
        self._result_label.set_name('result-label')
        if result:
            self.set_result_text(result)
        self._box.add(self._result_label)
        self._result_label.show()

        self.add(self._box)
        self._box.show()

    def set_name_text(self, name):
        self._name_label.set_text(name)

    def set_result_text(self, result):
        self._result_label.set_text(str(result))
