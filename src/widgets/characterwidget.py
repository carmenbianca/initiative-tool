# Copyright 2018  Carmen Bianca Bakker <carmen@carmenbianca.eu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0+

from gettext import gettext as _

from gi.repository import GObject, Gtk


class CharacterBox(Gtk.ListBox):
    __gtype_name__ = 'CharacterBox'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.model = None
        self._added = False

        # This button is always at the bottom.
        self._add_button = Gtk.Button()
        image = Gtk.Image.new_from_icon_name(
            'list-add-symbolic', Gtk.IconSize.BUTTON)
        self._add_button.set_image(image)
        self._add_button.set_relief(Gtk.ReliefStyle.NONE)
        self._add_button.props.halign = Gtk.Align.CENTER
        self._add_button.props.tooltip_text = _('Add another character')
        row = Gtk.ListBoxRow()
        row.set_can_focus(False)
        row.add(self._add_button)
        self.insert(row, -1)
        self._add_button.show()
        row.show()

        self.set_selection_mode(Gtk.SelectionMode.NONE)

        self._add_button.connect('clicked', self._add_entry_to_model)

    def set_model(self, model):
        # TODO: Make this remove the current rows.
        self.model = model

        self._setup_data()

        self.model.connect('row-changed', self._update_row)
        self.model.connect('row-inserted', self._insert_row)
        self.model.connect('row-deleted', self._delete_row)

    def _setup_data(self):
        for i, item in enumerate(self.model):
            # TODO: This stinks of duplication.  See _insert_row.
            character = CharacterWidget(item[0], item[1])
            character.connect('changed', self._character_changed)
            character.connect('deleted', self._character_deleted)
            self.insert(character, i)
            character.show()

    def _update_row(self, model, path, _):
        row = path.get_indices()[0]

        character = self.get_row_at_index(row).get_child()
        character.name = model[path][0]
        character.modifier = model[path][1]

    def _insert_row(self, model, path, _):
        row = path.get_indices()[0]

        item = model[path]
        character = CharacterWidget(item[0], item[1])
        character.connect('changed', self._character_changed)
        character.connect('deleted', self._character_deleted)

        self.insert(character, row)
        character.show()

        if self._added:
            self._added = False
            character._name.grab_focus()

    def _delete_row(self, model, path):
        row = path.get_indices()[0]

        row_widget = self.get_row_at_index(row)
        self.remove(row_widget)
        row_widget.destroy()

    def _add_entry_to_model(self, button):
        if self.model:
            self._added = True
            self.model.append(['', 0])

    def _character_changed(self, _, index, name, modifier):
        if self.model:
            self.model[index][0] = name
            self.model[index][1] = modifier

    def _character_deleted(self, _, index):
        iter = self.model.get_iter(Gtk.TreePath([index]))
        self.model.remove(iter)


@Gtk.Template(resource_path='/org/gnome/Initiative-Tool/ui/CharacterWidget.ui')
class CharacterWidget(Gtk.Box):
    __gtype_name__ = 'CharacterWidget'

    __gsignals__ = {
        'changed': (GObject.SIGNAL_RUN_FIRST, None, (int, str, int)),
        'deleted': (GObject.SIGNAL_RUN_FIRST, None, (int,)),
    }

    _name = Gtk.Template.Child()
    _modifier = Gtk.Template.Child()
    _delete = Gtk.Template.Child()

    def __init__(self, name='', modifier=0, **kwargs):
        super().__init__(**kwargs)

        self._modifier.set_range(-100, 100)

        self.name = name
        self.modifier = modifier

        self._name.connect('focus-in-event', self._name_focus_in)
        self._name.connect('focus-out-event', self._name_focus_out)

        self._name.connect('changed', self._changed)
        self._modifier.connect('value-changed', self._changed)
        self._delete.connect('clicked', self._deleted)

    @property
    def name(self):
        return self._name.get_text()

    @name.setter
    def name(self, value):
        self._name.set_text(value)

    @property
    def modifier(self):
        return self._modifier.get_value()

    @modifier.setter
    def modifier(self, value):
        self._modifier.set_value(value)

    def _changed(self, _):
        parent = self.get_parent()
        if parent:
            self.emit('changed', parent.get_index(), self.name, self.modifier)

    def _deleted(self, _):
        parent = self.get_parent()
        if parent:
            self.emit('deleted', parent.get_index())

    def _name_focus_in(self, widget, event):
        # self._name.props.has_frame = True
        pass

    def _name_focus_out(self, widget, event):
        # self._name.props.has_frame = False
        self._name.select_region(0, 0)
